# Projekt na przedmiot TRAK

## Skład osobowy:
- Sofia Krutko
- Piotr Tyl
- Łukasz Zawistowski
- Piotr Michta

## Quick Preview:
### Model bazowy:
![Model bazowy](base_character.png)

### Toon shader:
![Toon shader](toon_character.png)

### Draw shader:
![Draw shader](drawn_character.png)

Powyższe przykłady dostępne są w plikach `project_tester.blend` w folderach odpowiadających odpowiednim shader'om

W razie gdyby modele były czarne po pull'u należy przeładować jeszcze raz tesktury z modelu bazowego, który znajduje się w folderze `comic_toon`.
